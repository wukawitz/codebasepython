<!DOCTYPE html>
<html>
<head>
    <title>{{ page_title + " | " if page_title else "" }}{{ site_title }}</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/main.css">

    % if module_css:
        % for css in module_css:
            <link rel="stylesheet" href="{{css}}">
        % end
    % end

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    % if module_js:
        % for js in module_js:
            <script type="text/javascript" src="{{js}}"></script>
        % end
    % end

</head>
<body>
    <div id="application">
