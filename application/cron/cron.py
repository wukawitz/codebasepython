#!/usr/bin/env python

""" cron.py - provide cron services """

import sys
import os

# Set path for application module import
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../..")

# App
from application import *

# Cron related functionality goes here
