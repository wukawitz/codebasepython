#!/usr/bin/env python

""" decorators.py - methods to add additional functionality to the application """

# App
from application import *

def dall(func):
    """ Catchall decorator """
    def dall_wrapper(*args, **kwargs):
        # Stuff to go here
        return func(*args, **kwargs)
    return dall_wrapper

def stracker(func):
    """ Decorator for usage stats """
    def stracker_wrapper(*args, **kwargs):
        Log.alog(request.remote_addr +" - "+ request.url)
        return func(*args, **kwargs)
    return stracker_wrapper
