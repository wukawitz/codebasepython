#!/usr/bin/env python

""" database.py - class to communicate with the database """ 

# App
from application.config import config

# Native
import sqlite3

class Database(object):
    """ Database Handler """
    
    def __init__(self):
        """ Database constructor """
        self.autocommit = True
        self.connection = None
        self.cursor = None
    
    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d
    
    def connect(self):
        """ Connect to database """
        try:
            self.connection = sqlite3.connect(config.DATABASE)
            #self.connection.row_factory = sqlite3.Row
            self.connection.row_factory = self.dict_factory
            self.cursor = self.connection.cursor()
        except Exception, e:
            print "ERROR"
            print str(e)
        return self
    
    def runQuery(self, qry, params):
        """ Run parameterized query """
        data = []
        try:
            self.cursor.execute(qry, params)
            data = self.cursor.fetchall()
            if self.autocommit:
                self.connection.commit()
        except Exception, e:
            print "ERROR"
            print str(e)
        return data
        
    def runRawQuery(self, qry):
        """ Run raw query without parameters """
        data = []
        try:
            self.cursor.execute(qry)
            data = self.cursor.fetchall()
            if self.autocommit:
                self.connection.commit()
        except Exception, e:
            print "ERROR"
            print str(e)
        return data
    
    def getAffectedCount(self):
        """ Get the number of rows affected """
        return self.cursor.rowcount
    
    def getInsertId(self):
        """ Get the number of rows inserted """
        return self.cursor.lastrowid
        
    def setAutoCommitStatus(self, status):
        """ Set the autocommit status of queries (default is True) """
        self.autocommit = True
        return self
        
    def commitToDatabase(self):
        """ Manual commit of database """
        self.connection.commit()
        return self
        
    def escapeText(self, text):
        """ Clean text(remove spaces) and escape characters """
        text = text.encode("string_escape")
        text = text.strip()
        return text
        
    def unescapeText(self, text):
        """ Remove escape characters """
        text = text.decode("string_escape")
        return text
        