#!/usr/bin/env python

""" helper.py - static class with many utility methods """

# App
from application import *

class Helper(object):
    """ Static class with many utility methods """

    @staticmethod
    def renderView(temp, params=None):
        """ Render all views provided """
        if params == None:
            params = {}
        if type(temp) == str:
            if re.search(r"^modules", temp, re.M|re.I):
                return template("application/"+ temp, params)
            else:
                return template("application/views/"+ temp, params)
        elif type(temp) == list:
            rtn = ""
            for t in temp:
                if re.search(r"^modules", t, re.M|re.I):
                    rtn += template("application/"+ t, params)
                else:
                    rtn += template("application/views/"+ t, params)
            return rtn
        else:
            return "ERROR: Template rendering failed."

    @staticmethod
    def renderModuleView(temp, params=None):
        """ Render all views provided """
        rtn = ""
        if params == None:
            params = {}
        rtn += template("application/views/common/header.tpl", params)
        rtn += template("application/views/common/topnav.tpl", params)
        rtn += template("application/views/common/topwrap.tpl", params)
        rtn += template("application/"+ temp, params)
        rtn += template("application/views/common/bottomwrap.tpl", params)
        rtn += template("application/views/common/footer.tpl", params)
        return rtn

    @staticmethod
    def hash256(data):
        """ Create 256b hash from a data str """
        hash_object = hashlib.sha256(data)
        return hash_object.hexdigest()

    @staticmethod
    def strip_nonascii(text):
        """ Remove all non ascii characters from text """
        return ''.join([c if ord(c) < 128 else '' for c in text])

    @staticmethod
    def ip2long(ip):
        """ Convert IP to long int """
        return struct.unpack('!L', socket.inet_aton(ip))[0]

    @staticmethod
    def long2ip(longnum):
        """ Convert long int to IP """
        return socket.inet_ntoa(struct.pack('!I', int(longnum)))

    @staticmethod
    def jtom(s):
        """ Convert json string to dict or list """
        d = json.loads(s)
        return d

    @staticmethod
    def mtoj(m):
        """ Multi-dict to JSON """
        j = {}
        m.keys()
        for key in m.keys():
            key_list = m.getall(key)
            if len(key_list) is 1:
                j[key] = Helper.strip_nonascii(unicode(key_list[0].encode("utf-8"), "utf-8"))
            else:
                j[key] = Helper.strip_nonascii(unicode(key_list.encode("utf-8"), "utf-8"))
        return j

    @staticmethod
    def prettyd(dict, pre=False):
        """ Return python list and dictionary's in readable format """
        if pre:
            return '<pre>' + json.dumps(dict, default=Helper.fix_json, sort_keys=True, indent=4) + '</pre>'
        else:
            return json.dumps(dict, default=Helper.fix_json, sort_keys=True, indent=4)

    @staticmethod
    def fix_json(obj):
        """ Tidy up for JSON formatting """
        if type(obj) is datetime.date or type(obj) is datetime.datetime:
            return obj.isoformat()
        return None

    @staticmethod
    def randomvar(size=32, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
        """ Generate random string of chosen length """
        return "".join(random.choice(chars) for i in range(size))

    @staticmethod
    def pagination(urltype, base, current_page, max_pages, sortfield, sortdirection):
        """ Produce pagination urls """
        returnurl = "/"
        if urltype == "first_url":
            returnurl += base
            returnurl += "/"
            returnurl += "1"
            returnurl += "/"
            returnurl += sortfield
            returnurl += "/"
            returnurl += sortdirection
        elif urltype == "previous_url":
            returnurl += base
            returnurl += "/"
            if int(current_page) > 1:
                returnurl += str(int(current_page) - 1)
            else:
                returnurl += "1"
            returnurl += "/"
            returnurl += sortfield
            returnurl += "/"
            returnurl += sortdirection
        elif urltype == "current_url":
            returnurl += base
            returnurl += "/"
            returnurl += str(current_page)
            returnurl += "/"
            returnurl += sortfield
            returnurl += "/"
            returnurl += sortdirection
        elif urltype == "next_url":
            returnurl += base
            returnurl += "/"
            if int(current_page) < int(max_pages):
                returnurl += str(int(current_page) + 1)
            else:
                returnurl += str(current_page)
            returnurl += "/"
            returnurl += sortfield
            returnurl += "/"
            returnurl += sortdirection
        elif urltype == "last_url":
            returnurl += base
            returnurl += "/"
            returnurl += str(max_pages)
            returnurl += "/"
            returnurl += sortfield
            returnurl += "/"
            returnurl += sortdirection
        else:
            return returnurl
        return returnurl

    @staticmethod
    def bounceapp():
        """ Touch the wsgi and force a reload of code """
        subprocess.call(["touch", config.BASE_PATH +"app.py"])
