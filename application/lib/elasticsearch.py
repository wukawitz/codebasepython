#!/usr/bin/env python

""" elasticsearch.py - code to communicate with elasticsearch """

# App
from application import *

class Elasticsearch(object):
	""" Elastic Class """

	def __init__(self):
		""" Elastic constructor """
		self.con = None
		urllib3.disable_warnings()
		self.connect()

	def connect(self, url=config.ES_HOST):
		""" Connect to elastic instance """
		self.con = elasticsearch.Elasticsearch([url])

	def get(self, index=None, doc_type=None, doc_id=None):
		""" Get an elastic doc """
		try:
			if index and doc_type and doc_id:
				return self.con.get(index=index, doc_type=doc_type, id=doc_id)
			else:
				return None
		except Exception, e:
			return None

	def search(self, index=None, doc_type=None, body=None):
		""" Search an index """
		if not body:
			body = {"query": {"match_all": {}}}
		else:
			body = body
		try:
			if index and doc_type:
				return self.con.search(index=index, doc_type=doc_type, body=body)
			elif index:
				return self.con.search(index=index, body=body)
			else:
				return self.con.search(body=body)
		except Exception, e:
			return None

	def create(self, index=None, doc_type=None, doc_id=None, body=None):
		""" Create a doc """
		try:
			if index and doc_type and doc_id and body:
				return self.con.index(index=index, doc_type=doc_type, id=doc_id, body=body)
			elif index and doc_type and body:
				return self.con.index(index=index, doc_type=doc_type, body=body)
			else:
				return None
		except Exception, e:
			return None

	def update(self, index=None, doc_type=None, doc_id=None, body=None):
		""" Update a doc """
		try:
			if index and doc_type and doc_id and body:
				return self.con.index(index=index, doc_type=doc_type, id=doc_id, body=body)
			else:
				return None
		except Exception, e:
			return None

	def delete(self, index=None, doc_type=None, doc_id=None):
		""" Delete a doc """
		try:
			if index and doc_type and doc_id:
				return self.con.delete(index=index, doc_type=doc_type, id=doc_id)
			elif index and doc_type:
				return self.con.delete(index=index, doc_type=doc_type)
			elif index:
				return self.con.delete(index=index)
		except Exception, e:
			return None
