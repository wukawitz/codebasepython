#!/usr/bin/env python

""" hooks.py - static class to hook code into app """

# App
from application import *

class Hooks(object):
	""" Static class to hook code into app """

	@staticmethod
	def getModules():
		""" Load modules for hooking """
		modules = []
		m = ModulesModel()
		active_modules = m.getModules({"status":"active"})
		for mod in active_modules:
			try:
				mod_incl = "application.modules."+ mod["module_path"] +".plugins."+ mod["module_plugin"].replace(".py", "")
				temp = __import__(mod_incl, globals(), locals(), ["*"], -1)
				modules.append(temp)
			except Exception, e:
				pass
		return modules

	@staticmethod
	def hookAppStart():
		""" Hooks to be executed at the start of the application """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.appStart()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookAppEnd():
		""" Hooks to be executed at the end of the application """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.appEnd()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookHeaderStart():
		""" Hooks to be executed at the start of the header """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.headerStart()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookHeaderEnd():
		""" Hooks to be executed at the end of the header """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.headerEnd()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookSidebarStart():
		""" Hooks to be executed at the start of the sidebar """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.sidebarStart()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookSidebarEnd():
		""" Hooks to be executed at the end of the sidebar """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.sidebarEnd()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookMainStart():
		""" Hooks to be executed at the start of the main content """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.mainStart()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content

	@staticmethod
	def hookMainEnd():
		""" Hooks to be executed at the end of the main content """
		result_content = ""
		result_content_list = []
		active_modules = Hooks.getModules()
		for mod in active_modules:
			result = ""
			try:
				result = mod.mainEnd()
			except Exception, e:
				pass
			result_content_list.append(result)
		result_content = "".join(result_content_list);
		return result_content
