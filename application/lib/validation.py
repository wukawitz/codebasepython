#!/usr/bin/env python

""" validation.py - static class with many validation methods """

# App
from application import *

class Validation(object):
	""" Static class with many validation methods """

	@staticmethod
	def notEmptyStr(data):
		""" Check for stripped and empty string """
		if data.strip() != "":
			return True
		else:
			return False

	@staticmethod
	def notEmptyList(data):
		""" Check for empty list """
		if len(data) > 0:
			return True
		else:
			return False

	@staticmethod
	def notEmptyDict(data):
		""" Check for empty dictionary """
		if len(data.keys()) > 0:
			return True
		else:
			return False

	@staticmethod
	def isMatch(data, matchstr):
		""" Check for match """
		if data == matchstr:
			return True
		else:
			return False

	@staticmethod
	def minLength(data, length):
		""" Check for minimun length of string - at least """
		if len(data) >= length:
			return True
		else:
			return False

	@staticmethod
	def maxLength(data, length):
		""" Check for maximum length of string - less than """
		if len(data) <= length:
			return True
		else:
			return False

	@staticmethod
	def isInt(data):
		""" Check that input is an int """
		if type(data) == int:
			return True
		else:
			return False

	@staticmethod
	def isStr(data):
		""" Check that input is a str """
		if type(data) == str:
			return True
		else:
			return False

	@staticmethod
	def isList(data):
		""" Check that input is a list """
		if type(data) == list:
			return True
		else:
			return False

	@staticmethod
	def isDict(data):
		""" Check that input is a dict """
		if type(data) == dict:
			return True
		else:
			return False

	@staticmethod
	def greaterThan(data, number):
		""" Check for greater than """
		if data > number:
			return True
		else:
			return False

	@staticmethod
	def lessThan(data, number):
		""" Check for less than """
		if data < number:
			return True
		else:
			return False

	@staticmethod
	def validEmail(data):
		""" Check for valid email address """
		if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", data) != None:
			return True
		else:
			return False

	@staticmethod
	def validIp(data):
		""" Check for valid ip address """
		if re.match("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", data) != None:
			return True
		else:
			return False

	@staticmethod
	def all(data, methods):
		""" Check for validity of all methods """
		valid_result = False
		try:
			methodlist = methods.split("|")
			for method in methodlist:
				matchobj = re.match(r"((\w+)\[(\d{1,5})\])", method, re.I|re.M)
				matchobj2 = re.match(r"((\w+)=([a-zA-Z0-9\.\@]{1,100}))", method, re.I|re.M)
				if matchobj:
					method = matchobj.group(2)
					param = matchobj.group(3)
					if method.lower() == "minLength".lower():
						valid_result = Validation.minLength(data, int(param))
					elif method.lower() == "maxLength".lower():
						valid_result = Validation.maxLength(data, int(param))
					elif method.lower() == "greaterThan".lower():
						valid_result = Validation.greaterThan(data, int(param))
					elif method.lower() == "lessThan".lower():
						valid_result = Validation.lessThan(data, int(param))
				elif matchobj2:
					method = matchobj2.group(2)
					param = matchobj2.group(3)
					if method.lower() == "isMatch".lower():
						valid_result = Validation.isMatch(data, param)
				else:
					if method.lower() == "notEmptyStr".lower():
						valid_result = Validation.notEmptyStr(data)
					elif method.lower() == "notEmptyList".lower():
						valid_result = Validation.notEmptyList(data)
					elif method.lower() == "notEmptyDict".lower():
						valid_result = Validation.notEmptyDict(data)
					elif method.lower() == "isInt".lower():
						valid_result = Validation.isInt(data)
					elif method.lower() == "isStr".lower():
						valid_result = Validation.isStr(data)
					elif method.lower() == "isList".lower():
						valid_result = Validation.isList(data)
					elif method.lower() == "isDict".lower():
						valid_result = Validation.isDict(data)
					elif method.lower() == "validEmail".lower():
						valid_result = Validation.validEmail(data)
					elif method.lower() == "validIp".lower():
						valid_result = Validation.validIp(data)
				if not valid_result:
					break
		except Exception, e:
			valid_result = False
		return valid_result
