#!/usr/bin/env python

""" log.py - Log all activities and events """

# App
from application import *

class Log(object):
    """ Log all activity and events """

    @staticmethod
    def alog(message):
        """ Log user access """
        if config.LOGGING_STATUS:
            Log.checkAndRename(config.ACCESS_LOG)
            fo = open(config.ACCESS_LOG, "a")
            log_message =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") +"\t"+ message.strip() + "\n"
            fo.write(log_message);
            fo.close()

    @staticmethod
    def ilog(message):
        """ Log informational messages """
        if config.LOGGING_STATUS:
            Log.checkAndRename(config.INFORMATION_LOG)
            fo = open(config.INFORMATION_LOG, "a")
            log_message =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") +"\t"+ message.strip() + "\n"
            fo.write(log_message);
            fo.close()

    @staticmethod
    def elog(message):
        """ Log error messages """
        if config.LOGGING_STATUS:
            Log.checkAndRename(config.ERROR_LOG)
            fo = open(config.ERROR_LOG, "a")
            log_message =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") +"\t"+ message.strip() + "\n"
            fo.write(log_message);
            fo.close()

    @staticmethod
    def clog(message):
        """ Log cron related messages """
        if config.LOGGING_STATUS:
            Log.checkAndRename(config.CRON_LOG)
            fo = open(config.CRON_LOG, "a")
            log_message =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") +"\t"+ message.strip() + "\n"
            fo.write(log_message);
            fo.close()

    @staticmethod
    def checkAndRename(logfile):
        """ Rename the log file if it gets too big """
        try:
            if(os.path.isfile(logfile)):
                file_size = os.path.getsize(logfile)
                if file_size > config.LOGSIZE:
                    os.rename(logfile, logfile +"."+ str(int(time.time())))
        except Exception, e:
            pass
