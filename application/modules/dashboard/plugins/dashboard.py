#!/usr/bin/env python

""" dashboard.py - Dashboard module provides dashboard functionality """

# App
from application import *

@route("/dashboard")
@route("/")
@stracker
@dall
def dashboard():
    """ Routes all dashboard requests """
    return Helper.renderModuleView("modules/dashboard/views/dashboard.tpl", params)

@route("/module/dashboard/assets/<filepath:path>")
@stracker
@dall
def dashboard_server_static(filepath):
    """ Static file path """
    static_file_path = config.MODULE_PATH +"/dashboard/static/assets"
    return static_file(filepath, root=static_file_path)
