manifest = {
    "module_unique_id": "370a2865b22632c304424ae9ba417cef",
    "module_name": "Dashboard",
    "module_description": "Provides dashboard functionality",
    "module_version": "1.0.0",
    "module_author": "Kyle Wukawitz",
    "module_copyright": "Copyright 2015, All Rights Reserved",
    "module_license": "TBD",
    "module_path": "dashboard",
    "module_plugin": "dashboard",
    "module_sidebar_links": [
        {
            "title":"Dashboard",
            "url":"/dashboard",
            "icon":"fa fa-home"
        }
    ],
    "module_static_assets":
    [
        {
            "js": ["assets/js/dashboard.js"],
            "css": ["assets/css/dashboard.css"]
        }
    ],
    "module_install": [],
    "module_lock": "yes"
}
