#!/usr/bin/env python

""" modules_model.py - class to track application modules """

# App
from application import *

class ModulesModel(object):
    """ Module class to track application modules """

    def __init__(self):
        """ Modules constructor """
        self.db = Database()
        self.db.connect()

    def addModule(self, params, status="active", sort_order=1):
        """ Add a new module """
        qry = "INSERT INTO modules "
        inserted_col = []
        inserted_val = []
        new_params = []
        for key, val in params.iteritems():
            inserted_col.append(key)
            inserted_val.append(":"+ key)
            if key == "module_sidebar_links" or key == "module_static_assets" or key == "module_install":
                new_params.append(str(val))
            else:
                new_params.append(val)
        inserted_col.append("status")
        inserted_val.append(":status")
        new_params.append(status)
        inserted_col.append("sort_order")
        inserted_val.append(":sort_order")
        new_params.append(sort_order)
        inserted_col.append("lastmod_stamp")
        inserted_val.append("strftime('%Y-%m-%d %H:%M:%S', 'now')")
        inserted_col.append("create_stamp")
        inserted_val.append("strftime('%Y-%m-%d %H:%M:%S', 'now')")
        qry += "("+ ", ".join(inserted_col) +") VALUES ("+ ", ".join(inserted_val) +")"
        self.db.runQuery(qry, new_params)
        return self.db.getInsertId()

    def updateModuleByName(self, name, params):
        """ Update a module record by name """
        qry = "UPDATE modules SET "
        updated = []
        for key, val in params.iteritems():
            updated.append(key +"=:"+ key)
        updated.append("lastmod_stamp=strftime('%Y-%m-%d %H:%M:%S', 'now')")
        qry += ", ".join(updated)
        qry += " WHERE module_name=:name"
        params["name"] = name
        self.db.runQuery(qry, params)

    def updateModuleById(self, module_id, params):
        """ Update a module record by id """
        qry = "UPDATE modules SET "
        updated = []
        for key, val in params.iteritems():
            updated.append(key +"=:"+ key)
        updated.append("lastmod_stamp=strftime('%Y-%m-%d %H:%M:%S', 'now')")
        qry += ", ".join(updated)
        qry += " WHERE id=:module_id"
        params["module_id"] = module_id
        self.db.runQuery(qry, params)

    def updateModuleByUniqueId(self, module_unique_id, params):
        """ Update a module record by unique id """
        qry = "UPDATE modules SET "
        updated = []
        for key, val in params.iteritems():
            updated.append(key +"=:"+ key)
        updated.append("lastmod_stamp=strftime('%Y-%m-%d %H:%M:%S', 'now')")
        qry += ", ".join(updated)
        qry += " WHERE module_unique_id=:module_unique_id"
        params["module_unique_id"] = module_unique_id
        self.db.runQuery(qry, params)

    def getModules(self, params=None):
        """ Get module records from the db """
        qry = "SELECT * FROM modules "
        if params != None:
            filters = []
            for key, val in params.iteritems():
                filters.append(key +"=:"+ key)
            qry += " WHERE "+ " AND ".join(filters)
        else:
            params = {}
        qry += " ORDER BY sort_order ASC, create_stamp ASC"
        result = self.db.runQuery(qry, params)
        return result

    def deleteModuleById(self, module_id):
        """ Delete a module record by id """
        qry = "DELETE FROM modules WHERE id=:module_id "
        params = {}
        params["module_id"] = module_id
        self.db.runQuery(qry, params)

    def addModuleFromInit(self, module_unique_id, status="active", order=1):
        """ Add a module to the app from data within __init__.py """
        modules = ModulesModel.getModulesInitData()
        for module in modules:
            if module_unique_id == module["module_unique_id"]:
                self.addModule(module, status, order)

    def getMaxOrder(self):
        """ Get the max order number from installed modules """
        maxorder = 0
        qry = "SELECT MAX(sort_order) AS maxorder FROM modules"
        results = self.db.runRawQuery(qry)
        try:
            maxorder = int(results[0]["maxorder"])
        except Exception, e:
            pass
        return maxorder

    @staticmethod
    def maxOrder():
        """ Get the max order number from installed modules """
        m = ModulesModel()
        maxorder = m.getMaxOrder()
        return maxorder

    @staticmethod
    def getModulesInitData():
        """ Get module data from init manifest """
        modules = []
        for module in os.listdir(config.MODULE_PATH):
            if not re.search(r"pyc$", module) and not re.search(r"^__", module):
                module_init_path = config.MODULE_PATH +"/"+ module +"/__init__.py"
                fo = open(module_init_path, "r")
                data = fo.read()
                fo.close()
                try:
                    exec(data)
                    modules.append(manifest)
                except Exception, e:
                    pass
        return modules

    @staticmethod
    def getModuleAssets(current_module, asset_type):
        """ Get a module's front-end assets """
        assets = []
        m = ModulesModel()
        module_data = m.getModules({"module_plugin":current_module})
        try:
            asset_info = module_data[0]["module_static_assets"]
            asset_info = asset_info.replace("'", "\"")
            asset_info_dict = Helper.jtom(asset_info)
            asset_list = asset_info_dict[0][asset_type]
            for ass in asset_list:
                assets.append("/module/"+ current_module +"/"+ ass)
        except Exception, e:
            print "Error: "+ str(e)
        return assets
