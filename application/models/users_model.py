#!/usr/bin/env python

""" users_model.py - static class with many utility methods """

# App
from application import *

class UsersModel(object):
    """ User class to facilitate user functions """

    def __init__(self):
        """ Users constructor """
        self.db = Database()
        self.db.connect()

    def createUser(self, username, password, firstname, lastname, email, status, userlevel):
        """ Create a new user """
        qry = "INSERT INTO users (username, password, firstname, lastname, email, status, userlevel, lastmod_stamp, create_stamp) VALUES (:username, :password, :firstname, :lastname, :email, :status, :userlevel, strftime('%Y-%m-%d %H:%M:%S', 'now'), strftime('%Y-%m-%d %H:%M:%S', 'now'))"
        self.db.runQuery(qry, {"username": username, "password": Helper.hash256(password), "firstname": firstname, "lastname": lastname, "email": email, "status": status, "userlevel": userlevel})
        return self.db.getInsertId()

    def updateUserById(self, user_id, params):
        """ Update an existing user record """
        qry = "UPDATE users SET "
        updated = []
        for key, val in params.iteritems():
            updated.append(key +"=:"+ key)
        updated.append("lastmod_stamp=strftime('%Y-%m-%d %H:%M:%S', 'now')")
        qry += ", ".join(updated)
        qry += " WHERE id=:user_id"
        try:
            if params["password"]:
                params["password"] = Helper.hash256(params["password"])
        except Exception, e:
            pass
        params["user_id"] = user_id
        self.db.runQuery(qry, params)

    def getAllUsers(self, params=None):
        """ Get all users or filter on fields """
        qry = "SELECT * FROM users"
        if params != None:
            filters = []
            for key, val in params.iteritems():
                filters.append(key +"=:"+ key)
            qry += " WHERE "+ " AND ".join(filters)
        else:
            params = {}
        result = self.db.runQuery(qry, params)
        return result

    def isValidUser(self, username, password):
        """ Check if user is valid """
        qry = "SELECT * FROM users WHERE username=:username AND password=:password AND status='active'"
        result = self.db.runQuery(qry, {"username": username, "password": Helper.hash256(password)})
        try:
            if result[0]["id"]:
                return True
        except Exception, e:
            pass
        return False

    def getUserInfoFromId(self, user_id):
        """ Get user information from ID """
        qry = "SELECT * FROM users WHERE id=:user_id"
        result = self.db.runQuery(qry, {"user_id":user_id})
        try:
            if result[0]["id"]:
                return result[0]
        except Exception, e:
            pass
        return None

    def getUserInfoFromUsername(self, username):
        """ Get user information from username """
        qry = "SELECT * FROM users WHERE username=:username"
        result = self.db.runQuery(qry, {"username":username})
        try:
            if result[0]["id"]:
                return result[0]
        except Exception, e:
            pass
        return None
