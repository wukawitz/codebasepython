#!/usr/bin/env python

""" sessions_model.py - class to track user sessions """

# App
from application import *

class SessionsModel(object):
    """ Session class to track user sessions """

    def __init__(self):
        """ Sessions constructor """
        self.db = Database()
        self.db.connect()

    def isValidSession(self, session_key):
        """ Check for valid user session """
        qry = "SELECT * FROM sessions WHERE session_key=:session_key"
        result = self.db.runQuery(qry, {"session_key": session_key})
        try:
            if result[0]["id"]:
                return True
        except Exception, e:
            pass
        return False

    def createUserSession(self, user_id):
        """ Create user session """
        session_key = Helper.hash256(str(datetime.datetime.now()) + str(uuid.uuid4()))
        qry = "INSERT INTO sessions (user_id, session_key, lastmod_stamp, create_stamp) VALUES (:user_id, :session_key, strftime('%Y-%m-%d %H:%M:%S', 'now'), strftime('%Y-%m-%d %H:%M:%S', 'now'))"
        self.db.runQuery(qry, {"user_id": user_id, "session_key": session_key})
        return session_key

    def createUserSessionCookie(self, session_key):
        """ Create user session cookie """
        response.set_cookie(config.SESSION_COOKIE_NAME, session_key, secret=config.COOKIE_KEY, path="/")

    def deleteUserSessionCookie(self):
        """ Delete user session cookie """
        response.set_cookie(config.SESSION_COOKIE_NAME, "", secret=config.COOKIE_KEY, path="/", expires=(datetime.datetime.now() - datetime.timedelta(days=1)))

    def getCurrentSessionKey(self):
        """ Retrieve the current session key """
        return request.get_cookie(config.SESSION_COOKIE_NAME, secret=config.COOKIE_KEY)

    def getUserFromSession(self, session_key):
        """ Get user information from session key """
        qry = "SELECT * FROM sessions WHERE session_key=:session_key"
        result = self.db.runQuery(qry, {"session_key": session_key})
        try:
            if result[0]["user_id"]:
                u = Users()
                return u.getUserInfoFromId(result[0]["user_id"])
        except Exception, e:
            pass
        return None

    @staticmethod
    def checkForCurrentSessionSendTo(success_path=None, fail_path="/login"):
        """ Check for current valid session """
        session_key = request.get_cookie(config.SESSION_COOKIE_NAME, secret=config.COOKIE_KEY)
        s = Sessions()
        session_exists = s.isValidSession(session_key)
        if not session_key:
            if fail_path:
                redirect(fail_path)
        elif session_key:
            if success_path:
                redirect(success_path)
