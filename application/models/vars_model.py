#!/usr/bin/env python

""" vars_model.py - Class to add/update/delete/select application level variables """

# App
from application import *

class VarsModel(object):
    """ Vars class """

    def __init__(self):
        """ Assets constructor """
        self.db = Database()
        self.db.connect()

    def createVar(self, var_name, var_value, var_description):
        """ Create a new variable """
        qry = "INSERT INTO sitevars (var_name, var_value, var_description, lastmod_stamp, create_stamp) VALUES (:var_name, :var_value, :var_description, strftime('%Y-%m-%d %H:%M:%S', 'now'), strftime('%Y-%m-%d %H:%M:%S', 'now'))"
        self.db.runQuery(qry, {"var_name":var_name, "var_value": var_value, "var_description":var_description})

    def updateVarById(self, var_id, params):
        """ Update an existing variable record """
        qry = "UPDATE sitevars SET "
        updated = []
        for key, val in params.iteritems():
            updated.append(key +"=:"+ key)
        updated.append("lastmod_stamp=strftime('%Y-%m-%d %H:%M:%S', 'now')")
        qry += ", ".join(updated)
        qry += " WHERE id=:var_id"
        params["var_id"] = var_id
        self.db.runQuery(qry, params)

    def getAllVars(self):
        """ Get all variables """
        qry = "SELECT * FROM sitevars"
        result = self.db.runQuery(qry, {})
        return result

    def getVarById(self, var_id):
        """ Get an variable based on id """
        qry = "SELECT * FROM sitevars WHERE id=:var_id"
        result = self.db.runQuery(qry, {"var_id": var_id})
        if len(result) == 1:
            return result[0]
        else:
            return {}

    def getVarByVarname(self, var_name):
        """ Get an variable based on var_name """
        qry = "SELECT * FROM sitevars WHERE var_name=:var_name"
        result = self.db.runQuery(qry, {"var_name": var_name})
        if len(result) == 1:
            return result[0]
        else:
            return {}

    def getVarValueByVarname(self, var_name):
        """ Get an variable based on var_name """
        qry = "SELECT var_value FROM sitevars WHERE var_name=:var_name"
        result = self.db.runQuery(qry, {"var_name": var_name})
        if len(result) == 1:
            return result[0]["var_value"]
        else:
            return {}
