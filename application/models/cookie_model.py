#!/usr/bin/env python

""" cookie_model.py - class to create and fetch cookies """

# App
from application import *

class CookieModel(object):
    """ Cookie class to create and fetch cookies """

    @staticmethod
    def createCookie(cookie_name, cookie_value):
        """ Create cookie """
        response.set_cookie(cookie_name, cookie_value, secret=config.COOKIE_KEY, path="/")

    @staticmethod
    def deleteCookie(cookie_name):
        """ Delete cookie """
        response.set_cookie(cookie_name, "", secret=config.COOKIE_KEY, path="/", expires=(datetime.datetime.now() - datetime.timedelta(days=1)))

    @staticmethod
    def getCookieKey(cookie_name):
        """ Get a cookie """
        return request.get_cookie(cookie_name, secret=config.COOKIE_KEY)
