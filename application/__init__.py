# Thirdparty
from application.thirdparty.bottle import route, run, request, response, get, post, template, static_file

# Native
from Crypto.Cipher import AES
from Crypto import Random
import os
import json
import datetime
import math
import hashlib
import struct
import socket
import importlib
import re
import base64
import uuid
import subprocess
import threading
import time
import random
import string
import elasticsearch
import urllib3

# App
from application.config import config
from application.lib.log import Log
from application.lib.helper import Helper
from application.lib.validation import Validation
from application.lib.database import Database
from application.lib.elasticsearch import Elasticsearch
from application.models.vars_model import VarsModel
from application.models.users_model import UsersModel
from application.models.sessions_model import SessionsModel
from application.models.modules_model import ModulesModel
from application.models.cookie_model import CookieModel

# Decorators
from application.lib.decorators import dall
from application.lib.decorators import stracker

# Default Params
params = {
    "site_title": config.SITE_TITLE,
    "page_title": None,
    "module_css": None,
    "module_js": None
}

# Dynamically Load Active Modules
m = ModulesModel()
active_modules = m.getModules({"status":"active"})
for mod in active_modules:
    mod_incl = "application.modules."+ mod["module_path"] +".plugins."+ mod["module_plugin"].replace(".py", "")
    try:
        __import__(mod_incl, globals(), locals(), ["*"], -1)
    except Exception, e:
        pass

# Import Hooks
from application.lib.hooks import Hooks
