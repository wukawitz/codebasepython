#!/usr/bin/env python

""" Contains all config information for project """

# Native Imports
import os

# Project Status ("development", "testing", "production")
PROJECT_STATUS = "development"

# Site Title
SITE_TITLE = "Palmetto"

# Base Path
BASE_PATH = os.path.dirname(__file__) + "/../../"

# Library Path
LIBRARY_PATH = os.path.dirname(__file__) + "/../lib"

# Module Path
MODULE_PATH = os.path.dirname(__file__) + "/../modules"

# Static Path
STATIC_PATH = os.path.dirname(__file__) + "/../static"

# Data Path
DATA_PATH = os.path.dirname(__file__) + "/../data"

# Cron Path
CRON_PATH = os.path.dirname(__file__) + "/../cron"

# Logging Path
LOGGING_PATH = os.path.dirname(__file__) + "/../logs"

# Thirdparty Path
THIRDPARTY_PATH = os.path.dirname(__file__) + "/../thirdparty"

# Views Path
VIEWS_PATH = os.path.dirname(__file__) + "/../views"

# Logging Status (True, False)
LOGGING_STATUS = True

# Access Log
ACCESS_LOG = LOGGING_PATH + "/access.log"

# Error Log
ERROR_LOG = LOGGING_PATH + "/error.log"

# Information Log
INFORMATION_LOG = LOGGING_PATH + "/information.log"

# Cron Log
CRON_LOG = LOGGING_PATH + "/cron.log"

# Log File Size
LOGSIZE = 20000000

# Database
DATABASE = DATA_PATH + "/data.db"

# Session Cookie
SESSION_COOKIE_NAME = "session_cookie"

# Session Cookie Signing Key
COOKIE_KEY = "6E0786CE1369A663D00BABF2CD28E6695922F18AB7B3C369290830B427FF3B06"

# Password Salt
PASSWORD_SALT = "DAFCA30DE740AFF6493DAB2242A19B9070E5BDF43B2C319C4AB07E5F3D36548E"

# Pagination limit
PAGINATION_LIMIT = 10

# Pagination max
PAGINATION_MAX = 1000000

# Elasticsearch host
ES_HOST = "localhost"
