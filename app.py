#!/usr/bin/env python

""" app.py - starting point and routing for the application """

__author__ = "Kyle Wukawitz"
__copyright__ = "Copyright 2015, All Rights Reserved"
__license__ = "MIT - see license.txt"
__version__ = "0.0.1"

# Thirdparty
from application.thirdparty.bottle import route, run, static_file

# App
from application import *

@route("/")
@stracker
def test():
    return "Hello, World!"

@route("/assets/<filepath:path>")
@stracker
def base_server_static(filepath):
    return static_file(filepath, root=config.STATIC_PATH +"/assets")

run(host="0.0.0.0", port=8080, debug=True, reloader=True)
